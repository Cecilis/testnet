﻿using Microsoft.EntityFrameworkCore;
using Test.Data.Configuration;
using Test.Data.Entities;

namespace Test.Infrastructure.Repositories
{
    public class ClientRepository : IClientRepository, IDisposable
    {
        private TestDBContext _context;

        public ClientRepository()
        {
            _context = new TestDBContext();

        }
        public Client? GetById(int? id)
        {
            return _context.Clients.Include(c => c.Emails)
                                            .Include(c => c.Addresses)
                                            .Include(c => c.Phones)
                                            .Where(c => c.Id == id)
                                            .FirstOrDefault();
        }

        public async Task<Client?> GetByIdAsync(int? id)
        {
            return await _context.Clients.Include(c => c.Emails)
                                            .Include(c => c.Addresses)
                                            .Include(c => c.Phones)
                                            .Where(c => c.Id == id)
                                            .FirstOrDefaultAsync();
        }
        public IEnumerable<Client> GetAll()
        {
            return _context.Clients.ToList();
        }
        public async Task<IEnumerable<Client>?> GetAllAsync()
        {
            return await _context.Clients.Include(c => c.Emails)
                                            .Include(c => c.Addresses)
                                            .Include(c => c.Phones)
                                            .ToListAsync();
        }

        public void Insert(Client model)
        {
            _context.Clients.Add(model);
        }

        public void Update(Client client)
        {
            _context.Entry(client).State = EntityState.Modified;
        }

        public async Task<int> UpdateAsync(Client client)
        {
            _context.Entry(client).State = EntityState.Modified;
            return await _context.SaveChangesAsync();
        }

        public int Save()
        {
            return _context.SaveChanges();
        }

        public async Task<int> SaveAsync(Client client)
        {
            _context.Add(client);
            return await _context.SaveChangesAsync();
        }
        public void Delete(int? Id)
        {
            Client entity = _context.Clients.Find(Id);
            _context.Clients.Remove(entity);
        }

        public async Task<int> DeleteAsync(int id)
        {
            var client = await _context.Clients.FindAsync(id);
            _context.Clients.Remove(client);
            return await _context.SaveChangesAsync();
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    _context.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        public bool Exists(int id)
        {
            return _context.Clients.Any(e => e.Id == id);
        }

    }
}
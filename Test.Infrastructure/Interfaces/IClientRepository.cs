﻿using Test.Data.Entities;

namespace Test.Infrastructure
{
    public interface IClientRepository : IDisposable
    {
        Client? GetById(int? id);
        IEnumerable<Client> GetAll();
        Task<IEnumerable<Client>?> GetAllAsync();
        void Insert(Client client);
        void Update(Client client);
        Task<int> UpdateAsync(Client client);
        void Delete(int? Id);
        Task<int> DeleteAsync(int id);
        int Save();
        Task<int> SaveAsync(Client client);
    }
}
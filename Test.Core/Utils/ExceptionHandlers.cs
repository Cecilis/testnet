﻿using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;

namespace Test.Core.Utils
{
    public static class ExceptionHandler
    {
        public static (bool show, string code, string errorMessage) HandleException(Exception exception)
        {
            bool show = false;
            string code = string.Empty;
            string errorMessage = string.Empty;
            string exceptionMessage = GetExceptionMessage(exception); ;
            string fieldAffected = string.Empty;

            if (exception is DbUpdateConcurrencyException concurrencyEx)
            {
                //TODO: Review main exception cases and add them to this handler
                show = false;
                code = "001";
                errorMessage = "Concurrency. Details" + GetExceptionMessage(exception);
            }
            else if (exception is DbUpdateException dbUpdateEx)
            {
                if ((!(dbUpdateEx.InnerException.InnerException ?? dbUpdateEx.InnerException).Equals(null)))
                {
                    if ((dbUpdateEx.InnerException?.InnerException ?? dbUpdateEx.InnerException) is SqlException sqlException)
                    {
                        switch (sqlException.Number)
                        {
                            case 2627: // Unique constraint error
                                {
                                    var message = exceptionMessage;
                                    fieldAffected = message.Substring(message.IndexOf("column '") + 8, message.IndexOf("'", message.IndexOf("column '") + 9) - message.IndexOf("column '") - 8);
                                    errorMessage = "Only one " + fieldAffected + " can be registered with that value.";
                                    break;
                                }
                            case 547: // Constraint check violation - Foreight Key
                                {
                                    errorMessage = "An error has occurred. Seems like there are duplicate values.";
                                    break;
                                }
                            case 2601: // Duplicated key row error  or Constraint violation exception
                                {
                                    var parts = exceptionMessage.Split("'");
                                    fieldAffected = parts[1].Replace("dbo.", "");
                                    var duplicatedValue = parts.Last().Replace(". The duplicate key value is (", "").Replace(").", "");
                                    errorMessage = fieldAffected + ": " + duplicatedValue + " already registered.";
                                    break;
                                }
                            default:
                                {
                                    errorMessage = "Error saving. Details" + exceptionMessage;
                                    break;
                                }
                        };
                        show = true;
                        code = sqlException.Number.ToString();
                    }
                }
            }
            errorMessage ??= "Error saving " + fieldAffected + ". Details" + exceptionMessage;

            return (show, code, errorMessage);

        }

        private static string GetExceptionMessage(Exception exception)
        {
            return (exception.InnerException?.InnerException ?? exception.InnerException)?.Message ?? "No details";
        }

        private static string GetColumnAffected(string message)
        {
            var column = string.Empty;
            if (message.HaveValue())
                column = message.Substring(message.IndexOf("column '") + 8, message.IndexOf("'", message.IndexOf("column '") + 9) - message.IndexOf("column '") - 8);
            return column;
        }
    }
}

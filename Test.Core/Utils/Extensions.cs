﻿using System.Net.Mail;

namespace Test.Core.Utils
{
    public static class Extensions
    {

        public static bool HaveNotValue(this string? str)
        {
            return string.IsNullOrEmpty(str?.Trim());
        }
        public static bool HaveValue(this string? str)
        {
            return !string.IsNullOrEmpty(str?.Trim());
        }
        public static bool IsValidEmailAddress(this string? email)
        {
            try
            {
                if (email == null)
                    return false;
                var newMailAddress = new MailAddress(email);
                return newMailAddress.Address == email;
            }
            catch
            {
                return false;
            }
        }
        public static int WordCount(this string str)
        {
            return str.Split(new char[] { ' ', '.', '?' },
                             StringSplitOptions.RemoveEmptyEntries).Length;
        }
    }
}

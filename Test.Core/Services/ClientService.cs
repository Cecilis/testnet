﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using Test.Core.Models;
using Test.Data.Configuration;
using Test.Data.Entities;
using Test.Infrastructure.Repositories;

namespace Test.Core.Services
{
    public class ClientService
    {
        private readonly ClientRepository _clientRepository;
        private TestDBContext _context;
        private Mapper _mapperClient;
        public ClientService()
        {

            _clientRepository = new();

            _context = new TestDBContext();

            var _mapperClientConfig = new MapperConfiguration(cfg =>
            {
                cfg.AllowNullCollections = true;
                cfg.CreateMap<Client, ClientModel>()
                    .ForMember(e => e.Id, m => m.MapFrom(m => m.Id))
                    .ForMember(e => e.DNI, m => m.MapFrom(m => m.DNI))
                    .ForMember(e => e.Name, m => m.MapFrom(m => m.Name))
                    .ForMember(e => e.LastName, m => m.MapFrom(m => m.LastName))
                    .ForMember(e => e.BirthDate, m => m.MapFrom(m => m.BirthDate))
                    .ForMember(e => e.Emails, m => m.MapFrom(m => m.Emails))
                    .ForMember(e => e.Addresses, m => m.MapFrom(m => m.Addresses))
                    .ForMember(e => e.Phones, m => m.MapFrom(m => m.Phones))
                    .ReverseMap();
                cfg.CreateMap<Email, EmailModel>().ReverseMap();
                cfg.CreateMap<Address, AddressModel>().ReverseMap();
                cfg.CreateMap<Phone, PhoneModel>().ReverseMap();
            });

            _mapperClient = new Mapper(_mapperClientConfig);
        }

        public async Task<IEnumerable<ClientModel>> GetAllAsync()
        {
            var clientModel = await _clientRepository.GetAllAsync();
            return clientModel != null ? _mapperClient.Map<Client[], ClientModel[]>(clientModel.ToArray()).ToList() : new List<ClientModel>();
        }

        public async Task<ClientModel?> GetByIdAsync(int? id)
        {
            var clients = await _clientRepository.GetByIdAsync(id);
            return clients != null ? _mapperClient.Map<Client, ClientModel>(clients) : new ClientModel();
        }
        public async Task<int> CreateAsync(ClientModel client)
        {
            Client entity = _mapperClient.Map<ClientModel, Client>(client);
            _context.Add(entity);
            return await _context.SaveChangesAsync();
        }
        public async Task<int> UpdateAsync(ClientModel client)
        {
            Client clientToUpdate = _mapperClient.Map<ClientModel, Client>(client);

            SetEntityState<Email>(clientToUpdate.Emails, "Id", "EmailAddress");
            SetEntityState<Address>(clientToUpdate.Addresses, "Id", "Description");
            SetEntityState<Phone>(clientToUpdate.Phones, "Id", "Number");

            _context.Entry(clientToUpdate).State = EntityState.Modified;

            return await _context.SaveChangesAsync();
        }
        public async Task<int> DeleteAsync(int id)
        {
            var client = await _context.Clients.FindAsync(id);
            _context.Clients.Remove(client);
            return await _context.SaveChangesAsync();
        }
        public bool Exists(int id)
        {
            return _context.Clients.Any(e => e.Id == id);
        }
        protected void SetEntityState<T>(ICollection<T> list, string index, string property)
        {
            foreach (var item in list)
            {
                if (!GetPropValue(item, index).Equals(0))
                {
                    if (string.IsNullOrEmpty(GetPropValue(item, property.Trim())?.ToString()))
                        _context.Entry(item).State = EntityState.Deleted;
                    else
                        _context.Entry(item).State = EntityState.Modified;
                }
                else _context.Entry(item).State = EntityState.Added;
            }
        }
        private static object? GetPropValue(object src, string propName)
        {
            return src.GetType()?.GetProperty(propName)?.GetValue(src, null);
        }

    }
}

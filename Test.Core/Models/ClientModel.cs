﻿namespace Test.Core.Models
{
    public class ClientModel
    {
        public int Id { get; set; }

        public string DNI { get; set; } = string.Empty;

        public string Name { get; set; } = string.Empty;

        public string LastName { get; set; } = string.Empty;
        public DateTime BirthDate { get; set; } = DateTime.Now;
        public List<EmailModel> Emails { get; set; } = new List<EmailModel>();
        public List<AddressModel> Addresses { get; set; } = new List<AddressModel>();
        public List<PhoneModel> Phones { get; set; } = new List<PhoneModel>();
        public string ErrorMessage { get; set; } = string.Empty;
    }
}

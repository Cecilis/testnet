﻿namespace Test.Core.Models

{
    public class EmailModel
    {
        public int Id { get; set; }
        public string EmailAddress { get; set; } = string.Empty;
    }
}

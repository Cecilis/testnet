﻿using FluentValidation;
using Test.Core.Models;
using Test.Core.Utils;

namespace Test.Core.Validators
{
    public class ClientValidator : AbstractValidator<ClientModel>
    {
        public ClientValidator()
        {
            RuleFor(x => x.DNI)
                .NotNull().NotEmpty()
                .WithMessage("Required")
                .Matches(@"^[a-zA-Z0-9\s]*$").WithMessage("Only letters and numbers are allowed");
            RuleFor(x => x.Name)
                .NotNull().NotEmpty()
                .WithMessage("Required")
                .Matches(@"^[a-zA-ZñÑ\s]*$").WithMessage("Only latin letters allowed");
            RuleFor(x => x.LastName)
                .NotNull().NotEmpty()
                .WithMessage("Required")
                .Matches(@"^[a-zA-ZñÑ\s]*$").WithMessage("Only latin letters allowed");
            RuleFor(x => x.BirthDate)
                .NotNull().NotEmpty()
                .WithMessage("Required");

            When(x => x.Emails.Any(e => e.EmailAddress.HaveValue()), () =>
            {
                RuleFor(x => x.Emails).Must(x => x.Count(e => e.EmailAddress.HaveValue()) <= 2)
                                      .WithMessage("No more than 2 emails are allowed");


                RuleForEach(x => x.Emails).Where(e => e.EmailAddress.HaveValue())
                                          .Must(e => e.EmailAddress.IsValidEmailAddress())
                                          .WithMessage("Not valid");
            }).Otherwise(() =>
            {
                RuleFor(x => x.Addresses).Must(a => a.Any(c => c.Description.HaveValue()))
                                         .WithMessage("Must include at least one email or address");
            });

            RuleFor(x => x.Emails)
               .Must(x => !x.Where(e => e.EmailAddress.HaveValue())
                            .GroupBy(e => e.EmailAddress).Any(c => c.Count() > 1))
               .WithMessage("Duplicates are not allowed");

            RuleFor(x => x.Addresses)
              .Must(x => !x.Where(e => e.Description.HaveValue())
                           .GroupBy(a => a.Description.ToLower()).Any(c => c.Count() > 1))
              .WithMessage("Duplicates are not allowed");

            RuleFor(x => x.Phones)
              .Must(x => x.Any(p => p.Number.HaveValue()))
              .WithMessage("At least 1 phone must be added");

            RuleFor(x => x.Phones)
              .Must(x => !x.Where(e => e.Number.HaveValue())
                           .GroupBy(p => p.Number).Any(c => c.Count() > 1))
              .WithMessage("Duplicates are not allowed");
        }

    }
}
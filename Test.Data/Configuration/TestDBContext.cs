﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Test.Data.Entities;

namespace Test.Data.Configuration
{

    public class TestDBContext : DbContext
    {
        public TestDBContext() : base()
        {
        }
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            IConfigurationRoot configuration = new ConfigurationBuilder()
            .SetBasePath(Path.Combine(Directory.GetCurrentDirectory()))
            .AddJsonFile("appsettings.json")
            .Build();
            optionsBuilder.UseSqlServer(configuration.GetConnectionString("TestDBContext"));
        }

        public DbSet<Client> Clients { get; set; }
        public DbSet<Email> Emails { get; set; }
        public DbSet<Address> Addresses { get; set; }
        public DbSet<Phone> Phones { get; set; }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.Entity<Client>().HasKey(u => u.Id).HasName("PK_Client_Id");
            builder.Entity<Client>().Property(u => u.DNI).HasMaxLength(20);
            builder.Entity<Client>().HasIndex(u => u.DNI)
                                    .IsUnique().HasDatabaseName("IX_Client_DNI");
            builder.Entity<Client>().Property(u => u.Name).HasMaxLength(50);
            builder.Entity<Client>().Property(u => u.LastName).HasMaxLength(50);
            builder.Entity<Client>().Property(u => u.BirthDate).HasDefaultValue(DateTime.Now);
            builder.Entity<Client>().Property(u => u.CreatedAt).HasDefaultValue(DateTime.Now);

            builder.Entity<Email>().HasKey(u => u.Id).HasName("PK_Email_Id");
            builder.Entity<Email>().Property(u => u.EmailAddress).HasMaxLength(100);
            builder.Entity<Email>().HasIndex(u => new { u.ClientId, u.EmailAddress })
                                   .IsUnique().HasDatabaseName("FK_Client_Email_EmailAddress");
            builder.Entity<Address>().HasKey(u => u.Id).HasName("PK_Address_Id");
            builder.Entity<Address>().Property(u => u.Description).HasMaxLength(255);
            builder.Entity<Address>().HasIndex(u => new { u.ClientId, u.Description })
                                   .IsUnique().HasDatabaseName("FK_Client_Address_Description");
            builder.Entity<Phone>().HasKey(u => u.Id).HasName("PK_Phone_Id");
            builder.Entity<Phone>().Property(u => u.Number).HasMaxLength(100);
            builder.Entity<Phone>().HasIndex(u => new { u.ClientId, u.Number })
                       .IsUnique().HasDatabaseName("FK_Client_Phone_Number");

        }

    }
}
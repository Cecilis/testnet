﻿using System.ComponentModel.DataAnnotations;

namespace Test.Data.Entities
{
    public abstract class Base
    {
        [Key]
        public int Id { get; set; }
        public DateTime CreatedAt { get; set; } = DateTime.Now;

    }
}

﻿using Microsoft.EntityFrameworkCore;

namespace Test.Data.Entities
{
    [Index(nameof(ClientId), nameof(EmailAddress), IsUnique = true)]
    public class Email : Base
    {
        public string EmailAddress { get; set; } = string.Empty;
        public int ClientId { get; set; } = 0;
        public virtual Client Client { get; set; } = new Client();

    }
}

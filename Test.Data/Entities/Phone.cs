﻿using Microsoft.EntityFrameworkCore;

namespace Test.Data.Entities
{
    [Index(nameof(ClientId), nameof(Number), IsUnique = true)]
    public class Phone : Base
    {
        public string Number { get; set; } = string.Empty;
        public int ClientId { get; set; } = 0;
        public virtual Client Client { get; set; } = new Client();

    }
}

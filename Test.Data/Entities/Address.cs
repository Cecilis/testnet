﻿using Microsoft.EntityFrameworkCore;

namespace Test.Data.Entities
{
    [Index(nameof(ClientId), nameof(Description), IsUnique = true)]
    public class Address : Base
    {
        public string Description { get; set; } = string.Empty;
        public int ClientId { get; set; } = 0;
        public virtual Client Client { get; set; } = new Client();


    }
}
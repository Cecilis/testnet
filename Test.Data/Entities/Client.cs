﻿using System.ComponentModel.DataAnnotations;

namespace Test.Data.Entities
{

    public class Client : Base
    {

        [RegularExpression(@"^[a-zA-Z0-9\s]*$", ErrorMessage = "Not valid")]
        public string DNI { get; set; } = string.Empty;

        [RegularExpression(@"^[a-zA-ZñÑ\s]*$", ErrorMessage = "Not valid")]
        public string Name { get; set; } = string.Empty;

        [RegularExpression(@"^[a-zA-ZñÑ\s]*$", ErrorMessage = "Not valid")]
        public string LastName { get; set; } = string.Empty;

        [Required(ErrorMessage = "* Required", AllowEmptyStrings = false)]
        [DataType(DataType.Date)]
        public DateTime BirthDate { get; set; } = DateTime.Now;

        public virtual ICollection<Email> Emails { get; set; } = new List<Email>();
        public virtual ICollection<Address> Addresses { get; set; } = new List<Address>();
        public virtual ICollection<Phone> Phones { get; set; } = new List<Phone>();

    }
}

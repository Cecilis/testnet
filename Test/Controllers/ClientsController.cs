﻿using Microsoft.AspNetCore.Mvc;
using Test.Core.Models;
using Test.Core.Services;
using Test.Core.Utils;

namespace Test.Controllers
{
    public class ClientsController : Controller
    {
        private readonly ClientService _clientServicio;

        public ClientsController()
        {
            _clientServicio = new();
        }

        // GET: Clients
        public async Task<IActionResult> Index()
        {
            return View(await _clientServicio.GetAllAsync());
        }

        // GET: Clients/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
                return NotFound();

            var client = await _clientServicio.GetByIdAsync(id);

            if (client == null)
                return NotFound();

            return View(client);
        }

        // GET: Clients/CreateAsync
        public IActionResult Create()
        {
            ClientModel client = new();
            FillRows(client);
            return View(client);
        }

        // POST: Clients/CreateAsync
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(ClientModel client)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    ClearRows(client);
                    await _clientServicio.CreateAsync(client);
                    return RedirectToAction(nameof(Index));
                }
                return View(client);
            }
            catch (Exception ex)
            {
                (bool show, string code, string exMessage) = ExceptionHandler.HandleException(ex);
                ModelState.AddModelError("ErrorMessage", exMessage);
                FillRows(client);
                return View(client);
            }
        }


        // GET: Clients/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
                return NotFound();

            var client = await _clientServicio.GetByIdAsync(id);

            if (client.Equals(null))
                return NotFound();

            FillRows(client);
            return View(client);
        }

        // POST: Clients/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, ClientModel client)
        {
            if (id != client.Id)
                return NotFound();

            try
            {
                if (!ModelState.IsValid)
                    return View(client);

                ClearRows(client);
                var result = await _clientServicio.UpdateAsync(client);
                return RedirectToAction(nameof(Index));
            }

            catch (Exception ex)
            {
                (bool show, string code, string exMessage) = ExceptionHandler.HandleException(ex);
                FillRows(client);
                ModelState.AddModelError("ErrorMessage", exMessage);
                return View(client);
            }

        }

        // GET: Clients/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
                return NotFound();

            var client = await _clientServicio.GetByIdAsync((int)id);

            if (client == null)
                return NotFound();

            return View(client);
        }

        // POST: Clients/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var result = await _clientServicio.DeleteAsync(id);
            return RedirectToAction(nameof(Index));
        }

        private bool ClientExists(int id)
        {
            return _clientServicio.Exists(id);
        }

        private static void ClearRows(ClientModel client)
        {
            client.Emails = client.Emails.Where(x => !(x.Id == 0 && x.EmailAddress.HaveNotValue())).ToList();
            client.Addresses = client.Addresses.Where(x => !(x.Id == 0 && x.Description.HaveNotValue())).ToList();
            client.Phones = client.Phones.Where(x => !(x.Id == 0 && x.Number.HaveNotValue())).ToList();
        }

        private static void FillRows(ClientModel client)
        {
            for (var i = 1; client.Emails.Count < 2; i++)
                client.Emails.Add(new EmailModel { EmailAddress = string.Empty });
            for (var i = 1; client.Addresses.Count < 2; i++)
                client.Addresses.Add(new AddressModel { Description = string.Empty });
            for (var i = 1; client.Phones.Count < 2; i++)
                client.Phones.Add(new PhoneModel { Number = string.Empty });
        }

    }
}

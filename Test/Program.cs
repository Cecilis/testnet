using FluentValidation;
using FluentValidation.AspNetCore;
using Test.Core.Models;
using Test.Core.Validators;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.
builder.Services.AddControllersWithViews();

builder.Services.AddMvc().AddFluentValidation(config =>
                        {
                            config.DisableDataAnnotationsValidation = true;
                            config.ImplicitlyValidateChildProperties = true;
                        })
                        .AddViewOptions(options =>
                        {
                            options.HtmlHelperOptions.ClientValidationEnabled = false;
                        });

builder.Services.AddTransient<IValidator<ClientModel>, ClientValidator>();

/*builder.Services.AddDbContext<TestDBContext>(options =>
    options.UseSqlServer(builder.Configuration.GetConnectionString("TestDBContext")));*/

var app = builder.Build();

// Configure the HTTP request pipeline.
if (!app.Environment.IsDevelopment())
{
    app.UseExceptionHandler("/Home/Error");
    // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
    app.UseHsts();
}

app.UseHttpsRedirection();
app.UseStaticFiles();

app.UseRouting();

app.UseAuthorization();

app.MapControllerRoute(
    name: "default",
    pattern: "{controller=Clients}/{action=Index}/{id?}");

app.Run();
